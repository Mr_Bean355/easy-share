package com.sbg.poc.easyshare;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.sbg.poc.beneficiary.Beneficiary;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;

import javax.imageio.ImageIO;

/**
 * Created by mav on 2016/07/26.
 */

public class EasyShareParseUtil {

    public Beneficiary decodeQRCode(String path) {
        Map<DecodeHintType, Object> hintsMap = new EnumMap<DecodeHintType, Object>(DecodeHintType.class);
        hintsMap.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
        hintsMap.put(DecodeHintType.POSSIBLE_FORMATS, EnumSet.allOf(BarcodeFormat.class));
        hintsMap.put(DecodeHintType.PURE_BARCODE, Boolean.FALSE);

        File qrFile = new File(path);
        String resultString = null;
        try {
            resultString = decode(qrFile, hintsMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(resultString);

        Gson gson = new GsonBuilder().create();
        Beneficiary ben = gson.fromJson(resultString, Beneficiary.class);

        return ben;

    }

    private String decode(File whatFile, Map<DecodeHintType, Object> whatHints) throws Exception {
        // check the required parameters
        if (whatFile == null || whatFile.getName().trim().isEmpty()) {
            throw new IllegalArgumentException("File not found, or invalid file name.");
        }
        BufferedImage tmpBfrImage;
        try {
            tmpBfrImage = ImageIO.read(whatFile);
        } catch (IOException tmpIoe) {
            throw new Exception(tmpIoe.getMessage());
        }
        if (tmpBfrImage == null) {
            throw new IllegalArgumentException("Could not decode image.");
        }
        LuminanceSource tmpSource = new BufferedImageLuminanceSource(tmpBfrImage);
        BinaryBitmap tmpBitmap = new BinaryBitmap(new HybridBinarizer(tmpSource));
        MultiFormatReader tmpBarcodeReader = new MultiFormatReader();
        Result tmpResult;
        String tmpFinalResult;
        try {
            if (whatHints != null && !whatHints.isEmpty()) {
                tmpResult = tmpBarcodeReader.decode(tmpBitmap, whatHints);
            } else {
                tmpResult = tmpBarcodeReader.decode(tmpBitmap);
            }
            // setting results.
            tmpFinalResult = String.valueOf(tmpResult.getText());
        } catch (Exception tmpExcpt) {
            throw new Exception("BarCodeUtil.decode Excpt err - " + tmpExcpt.toString() + " - " + tmpExcpt.getMessage());
        }
        return tmpFinalResult;
    }

    public static void main(String[] args) throws IOException {
        EasyShareParseUtil es = new EasyShareParseUtil();
        es.decodeQRCode("/Users/mav/Source/Standardbank/Hackathon/qr_sample.png");
    }
}
