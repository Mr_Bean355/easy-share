package com.sbg.poc.easyshare.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.mike.easyshare.R;
import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

@SuppressWarnings("ConstantConditions")
public class ScanQrCodeActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView scannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code_scanner);
        scannerView = (ZXingScannerView) findViewById(R.id.scanner);
        scannerView.setResultHandler(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        scannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    @Override
    public void handleResult(Result result) {
        System.out.println("-- " + result.getResultMetadata().toString());
        setResult(RESULT_OK, new Intent().putExtra("RESULT_DATA", result.getText()));
        finish();
    }
}
