package com.sbg.poc.easyshare.generator;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.IntRange;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.util.EnumMap;
import java.util.Map;

public class QrCodeBitmapGenerator {
    private static final String TAG = QrCodeBitmapGenerator.class.getSimpleName();

    public static Bitmap generateCode(String toEncode, @IntRange(from = 1) int size) {
        Map<EncodeHintType, Object> hintMap = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
        hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        hintMap.put(EncodeHintType.MARGIN, 1);
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
        Bitmap bitmap = null;
        try {
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix byteMatrix = qrCodeWriter.encode(toEncode, BarcodeFormat.QR_CODE, size, size, hintMap);
            bitmap = convertBitMatrixToBitmap(byteMatrix);
        } catch (Exception e) {
            Log.e(TAG, "Unable to generator QR code bitmap: " + e);
        }
        return bitmap;
    }

    private static Bitmap convertBitMatrixToBitmap(BitMatrix matrix) {
        int height = matrix.getHeight();
        int width = matrix.getWidth();
        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                bmp.setPixel(x, y, matrix.get(x, y) ? Color.BLACK : Color.WHITE);
            }
        }
        return bmp;
    }
}
