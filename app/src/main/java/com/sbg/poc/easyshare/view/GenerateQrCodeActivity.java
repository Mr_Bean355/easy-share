package com.sbg.poc.easyshare.view;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.example.mike.easyshare.R;
import com.google.gson.Gson;
import com.sbg.poc.beneficiary.AmountCurrency;
import com.sbg.poc.beneficiary.Bank;
import com.sbg.poc.beneficiary.Beneficiary;
import com.sbg.poc.beneficiary.PaymentNotification;
import com.sbg.poc.easyshare.generator.QrCodeBitmapGenerator;

public class GenerateQrCodeActivity extends AppCompatActivity {
    private static final int BITMAP_SIZE = 256;

    private ImageView generatedQrCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_qr_code);
        generatedQrCode = (ImageView) findViewById(R.id.generated_qr_code);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        String beneficiaryJson = new Gson().toJson(buildDummyBeneficiary());
        Bitmap bitmap = QrCodeBitmapGenerator.generateCode(beneficiaryJson, BITMAP_SIZE);
        generatedQrCode.setImageBitmap(bitmap);
    }

    private static Beneficiary buildDummyBeneficiary() {
        Beneficiary ben = new Beneficiary();

        ben.setBeneficiaryName("Mav");
        ben.setBeneficiaryType("P");
        ben.setFromReference("My From Reference");
        ben.setToReference("Ben To Reference");
        Bank bank = new Bank();
        bank.setBankName("Standard Bank");
        bank.setBranchCode("011839");
        bank.setAccountNumber("011839666");
        ben.setBandDetails(bank);

        PaymentNotification notification = new PaymentNotification();
        notification.setType("SMS");
        notification.setValue("0829992211");
        ben.setPaymentNotification(notification);

        AmountCurrency amount = new AmountCurrency();
        amount.setCurrency("ZAR");
        amount.setAmount(Double.valueOf("56.11"));
        ben.setTransactionAmount(amount);
        return ben;
    }
}
