package com.sbg.poc.easyshare.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.mike.easyshare.R;

@SuppressWarnings("ConstantConditions")
public class MainActivity extends AppCompatActivity {
    public static final int REQUEST_CODE_CAMERA_PERMISSION = 1;

    private TextView results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.qr_generate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onGenerateClicked();
            }
        });
        findViewById(R.id.qr_scan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onScanClicked();
            }
        });
        findViewById(R.id.qr_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onShareClicked();
            }
        });
        results = (TextView) findViewById(R.id.qr_results);
    }

    private void onGenerateClicked() {
        startActivity(new Intent(this, GenerateQrCodeActivity.class));
    }

    private void onScanClicked() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CODE_CAMERA_PERMISSION);
        } else {
            showScanner();
        }
    }

    private void onShareClicked() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_EMAIL, "emailaddress@emailaddress.com");
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        sendIntent.putExtra(Intent.EXTRA_TEXT, "<a href=\"qr://someStuff\">Click</a>");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_CAMERA_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            showScanner();
        }
    }

    private void showScanner() {
        startActivityForResult(new Intent(this, ScanQrCodeActivity.class), 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            results.setText(getString(R.string.qr_results_prefix, data.getStringExtra("RESULT_DATA")));
            results.setVisibility(View.VISIBLE);
        }
    }
}
