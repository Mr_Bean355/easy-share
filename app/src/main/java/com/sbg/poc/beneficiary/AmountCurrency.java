package com.sbg.poc.beneficiary;

/**
 * Created by mav on 2016/07/25.
 */
public class AmountCurrency {
    private String currency;
    private Double amount;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
