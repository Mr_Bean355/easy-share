package com.sbg.poc.beneficiary;

/**
 * Created by mav on 2016/07/25.
 */
public class PaymentNotification {
    private String type;
    private String value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
