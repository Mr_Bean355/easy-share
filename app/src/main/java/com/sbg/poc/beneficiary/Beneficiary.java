package com.sbg.poc.beneficiary;

/**
 * Created by mav on 2016/07/25.
 */
public class Beneficiary {
    private String beneficiaryName;
    private String toReference;
    private String fromReference;
    private String beneficiaryType;
    private Bank bandDetails;
    private PaymentNotification paymentNotification;
    private AmountCurrency transactionAmount;

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getToReference() {
        return toReference;
    }

    public void setToReference(String toReference) {
        this.toReference = toReference;
    }

    public String getFromReference() {
        return fromReference;
    }

    public void setFromReference(String fromReference) {
        this.fromReference = fromReference;
    }

    public String getBeneficiaryType() {
        return beneficiaryType;
    }

    public void setBeneficiaryType(String beneficiaryType) {
        this.beneficiaryType = beneficiaryType;
    }

    public Bank getBandDetails() {
        return bandDetails;
    }

    public void setBandDetails(Bank bandDetails) {
        this.bandDetails = bandDetails;
    }

    public PaymentNotification getPaymentNotification() {
        return paymentNotification;
    }

    public void setPaymentNotification(PaymentNotification paymentNotification) {
        this.paymentNotification = paymentNotification;
    }

    public AmountCurrency getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(AmountCurrency transactionAmount) {
        this.transactionAmount = transactionAmount;
    }
}
